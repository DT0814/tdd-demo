package com.tuwc.luggage;

public class Cabinet {
    private Luggage luggage;
    public Ticket deposit(Luggage luggage) {
        this.luggage=luggage;
        return new Ticket();
    }

    public Luggage claim(Ticket ticket) {
        return luggage;
    }
}
