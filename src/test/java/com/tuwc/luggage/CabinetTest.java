package com.tuwc.luggage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CabinetTest {
    @Test
    void test_cabinet_deposit_return_ticket() {
        Cabinet cabinet = new Cabinet();
        Ticket ticket=cabinet.deposit(new Luggage());
        Assertions.assertNotNull(ticket);
    }

    @Test
    void test_cabinet_claim_return_luggage() {
        Cabinet cabinet = new Cabinet();
        Luggage luggage = new Luggage();
        Ticket ticket=cabinet.deposit(luggage);
        Assertions.assertEquals(luggage,cabinet.claim(ticket));
    }
}
